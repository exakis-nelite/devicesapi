using System;
using Xunit;
using Devices_API.Controllers;
using Devices_API.Services;
using Devices_API.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;

namespace Devices_API_Tests
{
    public class DeviceControllerTests
    {
        DeviceController _controller;
        SendtoBroker _sendtoBroker;
        Object device;

        public DeviceControllerTests()
        {
            _sendtoBroker = new SendtoBroker();
        }

        [Fact]
        public void Post_WhenCalled_ReturnsOkResult()
        {
            //Arrange
            string jsonstring = "{\"id\": \"string\",\"name\": \"string\",\"deviceType\": \"presenceSensor\"}";
            device = JsonConvert.DeserializeObject<Object>(jsonstring);
            // Act
            IActionResult okResult = _controller.Post(device);

            // Assert
            Assert.IsType<System.Net.HttpStatusCode>(okResult);
        }

        [Fact]
        public void Get_WhenCalled_ReturnsAllItems()
        {
            // Act
            //var okResult = _controller.Get().Result as OkObjectResult;

            // Assert
            //var items = Assert.IsType<List<ShoppingItem>>(okResult.Value);
            //Assert.Equal(3, items.Count);
        }
    }
}
