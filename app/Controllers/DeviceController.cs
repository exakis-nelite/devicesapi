﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using app.Models;
using Newtonsoft.Json;
using app.Services;


namespace app.Controllers
{
    [Route("v1/device")]
[ApiController]
public class DeviceController : ControllerBase
{
   
        // GET: v1/device
        [HttpGet]
    public void Get()
    {
       
    }

    // GET v1/device/5
    
    
    //##########################################################################################
    //##########################################################################################
    //##########################################################################################
    //################################### HERE #################################################
    //##########################################################################################
    //##########################################################################################
    //##########################################################################################
    //##########################################################################################
    
    [HttpGet("{id}")]
    public int Get(int id)
    {
        return id;
    }

    //##########################################################################################
    //##########################################################################################
    //##########################################################################################
    //##########################################################################################
    //##########################################################################################
    //##########################################################################################
    //##########################################################################################
    //##########################################################################################
    // POST v1/device
    [HttpPost]
    public IActionResult Post([FromBody]Object content)
    {
           
            try
            {
                var jsonstring = JsonConvert.SerializeObject(content);
                Device device = JsonConvert.DeserializeObject<Device>(jsonstring);
                
                String UUID = Guid.NewGuid().ToString(); //generate Unique ID
                device.id = UUID;
                if (String.IsNullOrEmpty(device.name))
                {
                    device.name = UUID.Substring(UUID.Length - 12); //if the device has no name, give him the last 12 characters of UUID as a name
                }
                var finalstring = JsonConvert.SerializeObject(device);
                var sender = new SendtoBroker { };
                sender.publish(finalstring, "DEVICES_REGISTRATIONS");

                return Created("Device Object", device);
            }
            catch
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status405MethodNotAllowed);
            }
    }
    [HttpPost("{DeviceId}/telemetry")]
    public IActionResult Post([FromBody]Object content, [FromRoute]string DeviceId)
    {
            try
            {
                var jsonstring = JsonConvert.SerializeObject(content);
                Telemetry telemetry = JsonConvert.DeserializeObject<Telemetry>(jsonstring);
                
                telemetryId telemetryid = new telemetryId();
                telemetryid.deviceId = DeviceId; //add the device id from the request url
                telemetryid.metricDate = telemetry.metricDate;
                telemetryid.deviceType = telemetry.deviceType;
                telemetryid.metricValue = telemetry.metricValue;
                var finalstring = JsonConvert.SerializeObject(telemetryid);
                //string queue = "mqtt-subscription-" + DeviceId + "-receiveqos1";
                var sender = new SendtoBroker { };
                sender.publish(finalstring, "DEVICES_METRICS");

                return Ok();
                //return Created("Device Object", telemetryid);
            }
            catch
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status405MethodNotAllowed);
            }
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    [HttpDelete("{id}")]
    public void Delete(int id)
    {
    }
}
}
