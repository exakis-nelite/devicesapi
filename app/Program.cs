﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using app.MQTT;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MQTTnet.AspNetCore;

namespace app
{
    public class Program
    {

        public static void Main(string[] args)
        {
            Thread t1 = new Thread((StartMqttServer));
            Thread t2 = new Thread(() => CreateWebHostBuilder(args).Build().Run());
            
            t1.Start();
            t2.Start();
           
        }

        public static void StartMqttServer()
        {
            MqttServer mqttServer = new MqttServer();
            mqttServer.RunMqttServer();
        }

        /*public static IWebHostBuilder BuildWebHost(string[] args) =>
                               WebHost.CreateDefaultBuilder(args)
                                   .UseKestrel(o => {
                                       o.ListenAnyIP(1883, l => l.UseMqtt()); // mqtt pipeline
                                       o.ListenAnyIP(5000); // default http pipeline
                                   })
                               .UseStartup<Startup>();*/


        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
