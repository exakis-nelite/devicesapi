﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MQTTnet;
using MQTTnet.Protocol;
using MQTTnet.Server;
using MQTTnet.Client.Options;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using app.Services;
using app.Models;
using MQTTnet.Client;
using Newtonsoft.Json;
using MQTTnet.Extensions.Rpc;

namespace app.MQTT
{
    public class MqttServer
{
        public void RunMqttServer()
        {
            string re1="(v1)";	// Variable Name 1
            string re2="(\\/)";	// Any Single Character 1
            string re3="(device)";	// Variable Name 2
            string re4="(\\/)";	// Any Single Character 2
            string re5="([A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12})";	// UUID 
            string re6="(\\/)";	// Any Single Character 3
            string re7="(telemetry)";	// Variable Name 3

            Regex r = new Regex(re1+re2+re3+re4+re5+re6+re7,RegexOptions.IgnoreCase|RegexOptions.Singleline);
            
            var optionsBuilder = new MqttServerOptionsBuilder().WithDefaultEndpointPort(1885)
                .WithConnectionValidator(c =>
                {
                    if (c.ClientId.Length < 31) //length of the UUID it has been given by POST response
                    {
                        c.ReturnCode = MqttConnectReturnCode.ConnectionRefusedIdentifierRejected;
                        return;
                    }

                    c.ReturnCode = MqttConnectReturnCode.ConnectionAccepted;
                })
                .WithDefaultEndpoint()
                .WithApplicationMessageInterceptor(context =>
                {
                    Match m = r.Match(context.ApplicationMessage.Topic); //verify if it matches v1/device/[UUID]/telemetry
                    if (m.Success)
                    {    
                        //context.ApplicationMessage.Payload = Encoding.UTF8.GetBytes(context.ApplicationMessage.Payload);
                        string payload = Encoding.UTF8.GetString(context.ApplicationMessage.Payload, 0,
                            context.ApplicationMessage.Payload.Length);
                        
                        telemetryId telemetryid = JsonConvert.DeserializeObject<telemetryId>(payload); //verify the format is correct
                        var sender = new SendtoBroker { };

                        Task.Run(() => { sender.publish(payload, "DEVICES_METRICS"); });

                        //sender.publish(payload, "telemetry");
                        //Thread.Sleep(300);


                        /*var message = new MqttApplicationMessageBuilder()
                            .WithTopic(string.Format("v1/device/{0}/response", mqttdevice.macAddress))
                            .WithPayload(uuid)
                            .WithExactlyOnceQoS()
                            .WithRetainFlag()
                            .Build();

                        await mqttClient.PublishAsync(message); //send the device its unique ID with a custom queue made with its MAC Adress*/

                    }
                   

                })
                .Build();
            
            // Setup client validator.
            //var options = new MqttServerOptions();

            var mqttServer = new MqttFactory().CreateMqttServer();
             mqttServer.StartAsync(optionsBuilder);
             mqttServer.GetClientStatusAsync();

             //await mqttServer.StopAsync();
        }
    }
}
