﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace app.Models
{
    public class Telemetry
{
        public string metricDate { get; set; }
        public string deviceType { get; set; }
        public string metricValue { get; set; }
    }
}
