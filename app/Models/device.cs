﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace app.Models
{


    public class Device
    {
        public string id { get; set; }
        public string name { get; set; }
        public string deviceType { get; set; }
    }
}
