namespace app.Models
{
    public class telemetryId
    {
        public string deviceId { get; set; }
        public string metricDate { get; set; }
        public string deviceType { get; set; }
        public string metricValue { get; set; }
    }
}