namespace app.Models
{
    public class MqttDevice
    {
        public string id { get; set; }
        public string name { get; set; }
        public DeviceType deviceType { get; set; }
        public string macAddress { get; set; }
    }
}