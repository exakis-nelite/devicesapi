﻿namespace app.Models
{

    public enum DeviceType
    {  temperatureSensor,
       brightnessSensor,
       humiditySensor,
       ledDevice,
       beeperDevice
    }
}