using System;
using Xunit;

namespace test
{
    public class UnitTest1
    {
        [Fact]
        public void ShouldSendMqttMessageToADevice()
        {
            Assert.Equal(1, 1);
        }
        [Fact]
        public void ShouldReceiveMqttMetric()
        {
            Assert.Equal(1, 1);
        }
        [Fact]
        public void ShouldReceiveHttpPostMetric()
        {
            Assert.Equal(1, 1);
        }
        [Fact]
        public void ShouldReceiveHttpPostDevice()
        {
            Assert.Equal(1, 1);
        }
        [Fact]
        public void ShouldReturnDeviceId()
        {
            Assert.Equal(1, 1);
        }
        [Fact]
        public void ShouldSendAMetricToBroker()
        {
            Assert.Equal(1, 1);
        }
        [Fact]
        public void ShouldCreateUUID()
        {
            Assert.Equal(1, 1);
        }
        [Fact]
        public void ShouldReturnBadRequest()
        {
            Assert.Equal(1, 1);
        }
        [Fact]
        public void ShouldReturnOkIfDataIsGood()
        {
            Assert.Equal(1, 1);
        }
        [Fact]
        public void ShouldVerifyMqttClientId()
        {
            Assert.Equal(1, 1);
        }
        [Fact]
        public void ShouldCreateNameIfNotExist()
        {
            Assert.Equal(1, 1);
        }
    }
}